package org.example;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;

import java.util.Arrays;
import java.util.List;

public class Map {
    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        DataSet<Person> personDataSource = env.fromCollection(
                Arrays.asList(
                        new Person(23, "Tom"),
                        new Person(75, "Michael")));


        //Suppose that you want to extract only the age field from every object of the collection. You can use the map()
        // transformation to get only a specific field of the Person class:

        List<Integer> ages = personDataSource
                .map(Person::getAge)
                .collect();

        System.out.println(ages);

    }
}
