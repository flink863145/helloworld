package org.example;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.Arrays;
import java.util.List;

public class WordCount {

    //Our next and final step is to group the tuples by their first elements (words) and then perform a sum aggregate
    // on the second element to produce a count of the word occurrences:

    public static DataSet<Tuple2<String, Integer>> startWordCount(
            //We are using three types of Flink transformations: flatMap(), groupBy(), and aggregate().
            ExecutionEnvironment env, List<String> lines) throws Exception {
        DataSet<String> text = env.fromCollection(lines);

        return text.flatMap(new LineSplitter())
                .groupBy(0)
                .aggregate(Aggregations.SUM, 1);
    }


    public static void main(String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        List<String> lines = Arrays.asList(
                "This is a first sentence",
                "This is a second sentence with a one word");

        DataSet<Tuple2<String, Integer>> result = startWordCount(env, lines);

        List<Tuple2<String, Integer>> collect = result.collect();
        System.out.println("collect ==> " + collect);

    }
}
