package org.example;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.ExecutionEnvironment;

import org.apache.flink.api.java.DataSet;

import org.apache.flink.api.java.tuple.Tuple2;

import java.util.List;

public class sort {

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        Tuple2<Integer, String> secondPerson = new Tuple2<>(4, "Tom");
        Tuple2<Integer, String> thirdPerson = new Tuple2<>(5, "Scott");
        Tuple2<Integer, String> fourthPerson = new Tuple2<>(200, "Michael");
        Tuple2<Integer, String> firstPerson = new Tuple2<>(1, "Jack");

        DataSet<Tuple2<Integer, String>> transactions = env.fromElements(
                fourthPerson, secondPerson, thirdPerson, firstPerson);


        // If you want to sort this collection by the first field of the tuple, you can use the sortPartitions() transformation:

        List<Tuple2<Integer, String>> sorted = transactions
                .sortPartition(new IdKeySelectorTransaction(), Order.ASCENDING)
                .collect();


        System.out.println(sorted);

    }


}
