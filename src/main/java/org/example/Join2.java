package org.example;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

import java.util.List;

public class Join2 {

    // When you have two datasets, you may want to join them on some id field. For this, you can use the join()
    // transformation
    //Let’s create collections of transactions and addresses of a user:

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // Read person file and generate tuples out of each string read
        DataSet<Tuple2<Integer, String>> personSet = env.readTextFile("/home/myvm/Flink/HelloWorld/src/main/resources/person")
                .map(new MapFunction<String, Tuple2<Integer, String>>()                                     //presonSet = tuple of (1  John)
                {
                    public Tuple2<Integer, String> map(String value)
                    {
                        String[] words = value.split(",");                                                 // words = [ {1} {John}]
                        return new Tuple2<Integer, String>(Integer.parseInt(words[0]), words[1]);
                    }
                });

        // Read location file and generate tuples out of each string read
        DataSet<Tuple2<Integer, String>> locationSet = env.readTextFile("/home/myvm/Flink/HelloWorld/src/main/resources/location").
                map(new MapFunction<String, Tuple2<Integer, String>>()
                {                                                                                                 //locationSet = tuple of (1  DC)
                    public Tuple2<Integer, String> map(String value)
                    {
                        String[] words = value.split(",");
                        return new Tuple2<Integer, String>(Integer.parseInt(words[0]), words[1]);
                    }
                });

        List<Tuple2<Tuple2<Integer, String>, Tuple2<Integer, String>>>
                joined2 = personSet.join(locationSet)
                .where(0) .equalTo(0).collect();

        System.out.println(joined2);


        Tuple3<Integer, String, String> address
                = new Tuple3<>(1, "5th Avenue", "London");
        DataSet<Tuple3<Integer, String, String>> addresses
                = env.fromElements(address, new Tuple3<>(12, "Rue des Francs Bourgeois", "Boisbriand")
        , new Tuple3<>(12, "Rio", "Brasil"), new Tuple3<>(1065, "Laval", "QC"));

        Tuple2<Integer, String> firstTransaction
                = new Tuple2<>(1, "Transaction_1");
        DataSet<Tuple2<Integer, String>> transactions
                = env.fromElements(firstTransaction, new Tuple2<>(12, "Transaction_2"));

        List<Tuple2<Tuple2<Integer, String>, Tuple3<Integer, String, String>>>
                joined = transactions.join(addresses)
                .where(new IdKeySelectorTransaction())
                .equalTo(new IdKeySelectorAddress())
                .collect();

        System.out.println(joined);



        // redo this using txt files!
        // redo this using kafka, 1 topic for addresses and 1 topic for transactions

    }
}
