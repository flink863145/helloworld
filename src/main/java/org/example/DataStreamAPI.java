package org.example;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class DataStreamAPI {

    static StreamExecutionEnvironment executionEnvironment
            = StreamExecutionEnvironment.getExecutionEnvironment();

    public static void main(String[] args) throws Exception {

        // could be kafka or any ather dta stream sources like socket, etc ...
        DataStream<String> dataStream = executionEnvironment.fromElements(
                "This is a first sentence",
                "This is a second sentence with a one word");

        SingleOutputStreamOperator<String> upperCase = dataStream.map(String::toUpperCase);

        upperCase.print();
        executionEnvironment.execute();

    }
}
