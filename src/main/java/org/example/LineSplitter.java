package org.example;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

import java.util.stream.Stream;


/*
As the first step in our solution, we create a LineSplitter class that splits our input into tokens (words),
collecting for each token a Tuple2 of key-value pairs. In each of these tuples, the key is a word found in the text,
and the value is the integer one (1).
This class implements the FlatMapFunction interface that takes String as an input and produces a Tuple2<String, Integer>:
*/

public class LineSplitter implements FlatMapFunction<String, Tuple2<String, Integer>> {

    @Override
    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
        Stream.of(value.toLowerCase().split("\\W+"))
                .filter(t -> t.length() > 0) ///
                .forEach(token -> out.collect(new Tuple2<>(token, 1)));
        //We call the collect() method on the Collector class to push data forward in the processing pipeline.
    }

}