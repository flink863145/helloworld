package org.example;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.List;

public class Join {

    // When you have two datasets, you may want to join them on some id field. For this, you can use the join()
    // transformation
    //Let’s create collections of transactions and addresses of a user:

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        Tuple3<Integer, String, String> address
                = new Tuple3<>(1, "5th Avenue", "London");
        DataSet<Tuple3<Integer, String, String>> addresses
                = env.fromElements(address, new Tuple3<>(12, "Rue des Francs Bourgeois", "Boisbriand")
        , new Tuple3<>(12, "Rio", "Brasil"), new Tuple3<>(1065, "Laval", "QC"));

        Tuple2<Integer, String> firstTransaction
                = new Tuple2<>(1, "Transaction_1");
        DataSet<Tuple2<Integer, String>> transactions
                = env.fromElements(firstTransaction, new Tuple2<>(12, "Transaction_2"));

        List<Tuple2<Tuple2<Integer, String>, Tuple3<Integer, String, String>>>
                joined = transactions.join(addresses)
                .where(new IdKeySelectorTransaction())
                .equalTo(new IdKeySelectorAddress())
                .collect();

        System.out.println(joined);


        // redo this using txt files!
        // redo this using kafka, 1 topic for addresses and 1 topic for transactions

    }
}
