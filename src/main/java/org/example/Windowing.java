package org.example;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

import java.time.ZonedDateTime;

public class Windowing {

    //When processing a stream of events in real-time, you may sometimes need to group events together and apply
    // some computation on a window of those events.
    //Suppose we have a stream of events, where each event is a pair consisting of the event number and the timestamp
    // when the event was sent to our system, and that we can tolerate events that are out-of-order but only if they
    // are no more than twenty seconds late.
    //For this example, let’s first create a stream simulating two events that are several minutes apart and define a
    // timestamp extractor that specifies our lateness threshold:
    public static void main (String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        SingleOutputStreamOperator<Tuple2<Integer, Long>> windowed
                = env.fromElements(
                        new Tuple2<>(16, ZonedDateTime.now().plusMinutes(25).toInstant().getEpochSecond()),
                        new Tuple2<>(15, ZonedDateTime.now().plusMinutes(2).toInstant().getEpochSecond()))
                .assignTimestampsAndWatermarks(
                        new BoundedOutOfOrdernessTimestampExtractor
                                <Tuple2<Integer, Long>>(Time.seconds(20)) {

                            @Override
                            public long extractTimestamp(Tuple2<Integer, Long> element) {
                                return element.f1 * 1000;
                            }
                        });

        SingleOutputStreamOperator<Tuple2<Integer, Long>> reduced = windowed
                .windowAll(TumblingEventTimeWindows.of(Time.seconds(5)))
                .maxBy(0, true);
        reduced.print();

    }
}
