package org.example;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;

import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class FilterReduce {
    public static void main(String[] args) throws Exception {

        //Note that when you launch the application on the local machine, it will perform processing on the local JVM.
        // Should you want to start processing on a cluster of machines, you would need to install Apache Flink on those
        // machines and configure the ExecutionEnvironment accordingly.
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        //You can create a DataSet from multiple sources, such as Apache Kafka, a CSV, a file or virtually any other data source.
        DataSet<Integer> amounts = env.fromElements(1, 29, 40, 50);

        //Once you create an instance of the DataSet class, you can apply transformations to it.
        //
        //Let’s say that you want to filter numbers that are above a certain threshold and next sum them all.
        // You can use the filter() and reduce() transformations to achieve this:

        int threshold = 30;
        List<Integer> collect = amounts
                .filter(a -> a > threshold)
                .reduce((integer, t1) -> integer + t1)
                .collect();
        // Note that the collect() method is a sink operation that triggers the actual data transformations.
        System.out.println(collect);
    }
}